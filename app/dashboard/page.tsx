"use client";

import styles from "./page.module.scss";
import React from "react";
import Header from "@/components/Header";
import { ICard } from "@/interfaces";
import CardList from "@/components/CardList";

const cards: ICard[] = [
  {
    name: "Квіти",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    image: {
      src: "https://images.unsplash.com/photo-1621155346337-1d19476ba7d6?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTh8fGltYWdlfGVufDB8fDB8fHww",
      alt: "Квіти",
    },
    rate: 2.4,
  },
  {
    name: "Підйом",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    image: {
      src: "https://st.depositphotos.com/1000765/2000/i/450/depositphotos_20002307-stock-photo-3d-small-cooperation.jpg",
      alt: "Підйом",
    },
    rate: 3.2,
  },
  {
    name: "Нянькет",
    description:
      "няняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняняня",
    image: {
      src: "https://kor.ill.in.ua/m/400x253/1376932.jpg",
      alt: "нянькет",
    },
    rate: 7.4,
  },
  {
    name: "ее реп",
    description:
      "Лорем ипсум долор сит амет, консектетур адиписсинг элит, сед до эюсмод темпор инцидидунт ут лаборе эт долоре магна аликва. Ут эним ад миним вэниам, квис ноструд эхзерцитатион улламко лаборис ниси ут аликуип экс эа коммодо консекват. Дуйс ауте ируре долор ин репрехендерит ин волуптате велит эссе циллум долоре эу фугиат нулла париатур. Эксцептур синт оккаэкат купидадат нон проидент, сунт ин кульпа кви оффиция десерунт моллит аним ид эст лаборум.",
    image: {
      src: "rap.jpg",
      alt: "е реп)",
      inPublic: true,
    },
    rate: 88,
  },
  {
    name: "Аніме",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    image: {
      src: "https://wikiwarriors.org/mediawiki/images/7/7c/%D0%A0%D0%B5%D1%8F_%D0%B0%D1%8F%D0%BD%D0%B0%D0%BC%D0%B8.jpg",
      alt: "мама...",
    },
    rate: 6.2,
  },
];

export default function Dashboard() {
  return (
    <>
      <Header />
      <main className={styles.main}>
        <CardList list={cards}></CardList>
      </main>
    </>
  );
}
