import styles from "./page.module.scss";
import React from "react";
import Header from "@/components/Header";

export default function Home() {
  return (
    <>
      <Header />
      <main className={styles.main}>
        <div className={styles.centered}>
          <h1>це головна сторінка</h1>
          <span>але ви можете перейти на сторінку логіна зверху</span>
        </div>
      </main>
    </>
  );
}
