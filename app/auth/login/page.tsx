"use client";

import styles from "./page.module.scss";
import Link from "next/link";
import { useState } from "react";
import Input from "@/components/Input";
import { useRouter } from "next/navigation";

export default function Login() {
  const router = useRouter();

  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  return (
    <main className={styles.main}>
      <div className={styles.login}>
        <Link href={"/"} className={styles.back}>
          <span className={styles.arrow}>&lt;-</span> на головну
        </Link>
        <h1 className={styles.header}>Вхід</h1>
        <Input
          placeholder={"Ел. пошта"}
          required={true}
          onInput={(event) => {
            setEmail(event.target.value);
          }}
        >
          {email}
        </Input>
        <Input
          placeholder={"Пароль"}
          required={true}
          type={"password"}
          onInput={(event) => {
            setPassword(event.target.value);
          }}
        >
          {password}
        </Input>
        <button
          onClick={() => {
            alert(
              `Ел. пошта: ${email}\nПароль: ${password}\n\nЗараз ви будете перенаправлені на дешборд`,
            );
            router.push("/dashboard/");
          }}
          className={styles.button}
        >
          Увійти
        </button>
      </div>
    </main>
  );
}
