import { ChangeEvent, HTMLInputTypeAttribute } from "react";

export interface InputProps {
  placeholder: string;
  children?: string;
  onInput?: (event: ChangeEvent<HTMLInputElement>) => void;
  type?: HTMLInputTypeAttribute;
  required?: boolean;
}

export interface CardListProps {
  list?: ICard[];
}

export interface ICardImage {
  src: string;
  alt?: string;
  inPublic?: boolean;
}

export interface ICard {
  name: string;
  description: string;
  rate: number;
  image?: ICardImage;
}
