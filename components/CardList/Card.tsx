import { ICard } from "@/interfaces";
import styles from "./Card.module.scss";
import { useState } from "react";

export default function Card(props: ICard) {
  const [descriptionIsOpened, setDescriptionIsOpened] =
    useState<boolean>(false);

  return (
    <div className={styles.card}>
      <img
        className={styles.image}
        src={props.image?.src}
        alt={props.image?.alt}
      />
      <div className={styles.info}>
        <span className={styles.heading}>{props.name}</span>
        <span
          onClick={() => setDescriptionIsOpened(!descriptionIsOpened)}
          style={{
            whiteSpace: descriptionIsOpened ? "initial" : "nowrap",
          }}
          className={styles.description}
        >
          {props.description}
        </span>
        <span className={styles.rate}>
          оцінка:
          <b>{props.rate}</b>
        </span>
      </div>
    </div>
  );
}
