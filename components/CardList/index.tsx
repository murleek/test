import { CardListProps } from "@/interfaces";
import styles from "./CardList.module.scss";
import Card from "@/components/CardList/Card";

export default function CardList(props: CardListProps) {
  return (
    <div className={styles.cardList}>
      {props.list?.map((card) => <Card {...card} />)}
    </div>
  );
}
