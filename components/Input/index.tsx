import styles from "./Input.module.scss";
import { Inter } from "next/font/google";
import { InputProps } from "@/interfaces";

const inter = Inter({ subsets: ["latin"] });

export default function Input(props: InputProps) {
  return (
    <input
      value={props.children}
      required={props.required}
      type={props.type}
      onInput={props.onInput}
      className={inter.className + " " + styles.input}
      placeholder={props.placeholder}
    />
  );
}
