import styles from "./Header.module.scss";
import Link from "next/link";

export default function Header() {
  return (
    <div className={styles.wrap}>
      <div className={styles.header}>
        <div>
          <Link className={styles.heading} href={"/"}>
            Test exercise
          </Link>
        </div>
        <div>
          <Link className={styles.button} href={"/auth/login"}>
            Вхід
          </Link>
        </div>
      </div>
    </div>
  );
}
